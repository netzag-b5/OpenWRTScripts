# Tool for building custom openWRT images
Utilise the [openWRT build system](https://openwrt.org/docs/guide-developer/toolchain/use-buildsystem) to bring custom firmware to the access points in the dormitory Bärenstraße 5.

## Features
- Small openWRT image
- Install the **openWISP** client per default
- Config on the fly using a fancy Python wrapper
    - reducing the threat of typos

## Usage
**Use Linux to build the image!**
1. Install openWRT build dependencies: https://openwrt.org/docs/guide-developer/toolchain/install-buildsystem
2. Install python dependencies: `python -m pip install -r requirements.txt`
3. Update the openWRT version if necessary. See https://git.openwrt.org/?p=openwrt/openwrt.git;a=tags to find out about the latest version tags.
4. Run the wrapper: `python3 runMe.py`

## Remarks
- Tested with Debian 11.2 ("Bullseye").
- IPv6 is disabled by default. See https://openwrt.org/docs/guide-user/additional-software/saving_space to get an idea on how to enable it again.