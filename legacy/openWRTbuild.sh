#!/bin/bash
# https://openwrt.org/docs/guide-developer/toolchain/use-buildsystem

# Variables

# Image
arch="ath79"
subtarget="generic"
device="tplink_archer-c6-v2"
# OpenWISP
url=""
sharedSecret=""
# IP
apNumber=1 # TODO change
ip="192.168.200.${apNumber}"
# SSID
ssid="baernet-AP-${apNumber}"

# End Variables

# Download and update the sources
git clone https://git.openwrt.org/openwrt/openwrt.git openwrt
cd openwrt || exit

#git clean -d -x -f

git pull
 
# Select a specific code revision
git branch -a
git tag | cat
git checkout v21.02.2

# Add OpenWISP parameters
mkdir -p files/etc/config
cat << EOF > files/etc/config/openwisp # TODO Change in Production
config controller 'http'
    option url '${url}'
    option shared_secret '${sharedSecret}'
    option verify_ssl '0' # TODO Change in production when we do not use self signed certificates anymore
EOF

# Add UCI defaults
mkdir -p files/etc/uci-defaults
# TODO sed variables
cp ../uci-defaults/* files/etc/uci-defaults/

# Configure feeds
echo "src-git openwisp https://github.com/openwisp/openwisp-config.git" > feeds.conf
cat feeds.conf.default >> feeds.conf
# Update the feeds
./scripts/feeds update -a
./scripts/feeds install -a
 
# Configure Image
echo "CONFIG_TARGET_${arch}=y" > .config
echo "CONFIG_TARGET_${arch}_${subtarget}=y" >> .config
echo "CONFIG_TARGET_${arch}_${subtarget}_DEVICE_${device}=y" >> .config
echo "CONFIG_KERNEL_KALLSYMS=n" >> .config

# Remove unnecessary packages (https://openwrt.org/docs/guide-user/additional-software/saving_space)
# Remove pppoe
echo "CONFIG_PACKAGE_ppp=n" >> .config
echo "CONFIG_PACKAGE_ppp-mod-pppoe=n" >> .config
#Remove IPv6
echo "CONFIG_IPV6=n" >> .config
echo "CONFIG_PACKAGE_ip6tables=n" >> .config
echo "CONFIG_PACKAGE_odhcp6c=n" >> .config
echo "CONFIG_PACKAGE_kmod-ipv6=n" >> .config
echo "CONFIG_PACKAGE_kmod-ip6tables=n" >> .config
echo "CONFIG_PACKAGE_odhcpd-ipv6only=n" >> .config
# Remove DHCP server
echo "CONFIG_PACKAGE_dnsmasq=n" >> .config
# Remove iptables
echo "CONFIG_PACKAGE_iptables=n" >> .config
# End Remove unnecessary packages

# Remove wpad-basic-wolfssl and install hostapd
echo "CONFIG_PACKAGE_wpad-basic-wolfssl=n" >> .config
echo "CONFIG_PACKAGE_hostapd=y" >> .config
# Install OpenWISP
echo "CONFIG_PACKAGE_openwisp-config=y" >> .config
# Install LuCI
echo "CONFIG_PACKAGE_luci=y" >> .config

echo ".config!!!!!!!!!"
cat .config

make -j $(($(nproc)+1)) defconfig download clean world
