#!/bin/bash
# https://openwrt.org/docs/guide-developer/toolchain/use-buildsystem

# Variables

# Image
arch="{{ ap.openWRTarch }}"
subtarget="{{ ap.openWRTsubtarget }}"
device="{{ ap.openWRTname }}"
# OpenWISP
url="{{ openWISP_url}}"
sharedSecret="{{ openWISP_secret }}"
verifySSL={{ openWISP_verifySSL }}
# Misc
luciEnabled={{ luciEnabled }}

# Download and update the sources
git clone https://git.openwrt.org/openwrt/openwrt.git openwrt
cd openwrt || exit

#git clean -d -x -f

git pull
 
# Select a specific code revision
git branch -a
git tag | cat
git checkout v22.03.2 # UPDATE ME

# Add OpenWISP parameters
mkdir -p files/etc/config
cat << EOF > files/etc/config/openwisp
config controller 'http'
    option url '${url}'
    option shared_secret '${sharedSecret}'
    option tags '{{ ap.openWISPtag }}'
# Intermediate fix for ping
    option management_interface 'eth0.1001'
    option verify_ssl '1'
    option capath '/etc/ssl/certs/'
    option cacert '/etc/ssl/certs/b5CAcert.pem'
EOF

# Add UCI defaults
mkdir -p files/etc/uci-defaults
rm -rf files/etc/uci-defaults/*
cp ../rendered/uci-defaults/99_baer files/etc/uci-defaults/

# Add certificate
mkdir -p files/etc/ssl/certs
cp ../certs/* files/etc/ssl/certs/

# Configure feeds
echo "src-git openwisp https://github.com/openwisp/openwisp-config.git" > feeds.conf
echo "src-git openwisp_monitoring https://github.com/openwisp/openwrt-openwisp-monitoring.git" >> feeds.conf
cat feeds.conf.default >> feeds.conf
# Update the feeds
./scripts/feeds update -a
./scripts/feeds install -a
 
# Configure Image
echo "CONFIG_TARGET_${arch}=y" > .config
echo "CONFIG_TARGET_${arch}_${subtarget}=y" >> .config
echo "CONFIG_TARGET_${arch}_${subtarget}_DEVICE_${device}=y" >> .config
echo "CONFIG_KERNEL_KALLSYMS=n" >> .config

# Remove unnecessary packages (https://openwrt.org/docs/guide-user/additional-software/saving_space)
# Remove pppoe
echo "CONFIG_PACKAGE_ppp=n" >> .config
echo "CONFIG_PACKAGE_ppp-mod-pppoe=n" >> .config
#Remove IPv6
echo "CONFIG_IPV6=n" >> .config
echo "CONFIG_PACKAGE_ip6tables=n" >> .config
echo "CONFIG_PACKAGE_odhcp6c=n" >> .config
echo "CONFIG_PACKAGE_kmod-ipv6=n" >> .config
echo "CONFIG_PACKAGE_kmod-ip6tables=n" >> .config
echo "CONFIG_PACKAGE_odhcpd-ipv6only=n" >> .config
# Remove DHCP server
echo "CONFIG_PACKAGE_dnsmasq=n" >> .config
# Remove nftables
echo "CONFIG_PACKAGE_nftables=n" >> .config
# Remove firewall
echo "CONFIG_PACKAGE_firewall4=n" >> .config
# End Remove unnecessary packages

# Remove wpad-basic-wolfssl and install hostapd
echo "CONFIG_PACKAGE_wpad-basic-wolfssl=n" >> .config
echo "CONFIG_PACKAGE_hostapd=y" >> .config
# Install OpenWISP
echo "CONFIG_PACKAGE_openwisp-config=y" >> .config
echo "CONFIG_PACKAGE_netjson-monitoring=y" >> .config
echo "CONFIG_PACKAGE_openwisp-monitoring=y" >> .config
# Install LuCI, but without firewall
echo "CONFIG_PACKAGE_luci=n" >> .config
echo "CONFIG_PACKAGE_luci-base=${luciEnabled}" >> .config
# echo "CONFIG_PACKAGE_luci-ssl=${luciEnabled}" >> .config # Enabling luci-ssl also enables luci which then enables firewall
echo "CONFIG_PACKAGE_luci-mod-admin-full=${luciEnabled}" >> .config
echo "CONFIG_PACKAGE_luci-theme-bootstrap=${luciEnabled}" >> .config
echo "CONFIG_PACKAGE_uhttpd-mod-ubus=${luciEnabled}" >> .config
echo "CONFIG_PACKAGE_uhttpd=${luciEnabled}" >> .config

echo ".config!"
cat .config

# make -j $(($(nproc)+1)) defconfig download clean world
make -j $(($(nproc)+1)) defconfig download world