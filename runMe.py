import inquirer
import jinja2
import os
import stat
import subprocess


class AccessPoint:
    def __init__(self, visualName, openWRTname, openWISPtag, openWRTarch, openWRTsubtarget,
                 numberOfRadios):
        self.visualName = visualName
        self.openWRTname = openWRTname
        self.openWISPtag = openWISPtag
        self.openWRTarch = openWRTarch
        self.openWRTsubtarget = openWRTsubtarget
        self.numberOfRadios = numberOfRadios


builderScriptPath = os.path.join('rendered', 'makeOpenWRT.sh')

aps = [
    AccessPoint(
        "TP-Link Archer C6 v2 (EU)",
        "tplink_archer-c6-v2",
        "archerc6v2",
        "ath79",
        "generic",
        2),
    AccessPoint(
        "TP-Link TL-WR1043N v5",
        "tplink_tl-wr1043n-v5",
        "1043Nv5",
        "ath79",
        "generic",
        1),
    AccessPoint(
        "TP-Link TL-WR1043ND v4.x",
        "tplink_tl-wr1043nd-v4",
        "1043NDv4",
        "ath79",
        "generic",
        1),
    AccessPoint(
        "TP-Link TL-WR1043ND v3.x",
        "tplink_tl-wr1043nd-v3",
        "1043NDv3",
        "ath79",
        "generic",
        1),
    AccessPoint(
        "TP-Link TL-WR1043ND v2.x",
        "tplink_tl-wr1043nd-v2",
        "1043NDv2",
        "ath79",
        "generic",
        1),
    AccessPoint(
        "TP-Link TL-WR1043ND v1.x",
        "tplink_tl-wr1043nd-v1",
        "1043NDv1",
        "ath79",
        "generic",
        1)]


def getUserInputForBuild():
    choicesList = []
    for ap in aps:
        choicesList.append((ap.visualName, ap.openWRTname))

    questions = [
        inquirer.List(
            'ap',
            message="For which router model do you want to build openWRT?",
            choices=choicesList
        ),
        inquirer.Text(
            'openWispUrl',
            message="Enter the openWISP URL.",
            default="https://openwisp2.baer.rwth-aachen.de"
        ),
        inquirer.Password(
            'openWispSecret',
            message="Enter the openWISP shared secret."
        ),
        inquirer.Confirm(
            'luci',
            message="Install LuCI?",
            default=True
        )
    ]
    answers = inquirer.prompt(questions)

    if answers['luci']:
        luciEnabled = "y"
    else:
        luciEnabled = "n"

    for ap in aps:
        if ap.openWRTname == answers['ap']:
            print(answers)
            return ap, answers['openWispUrl'], answers['openWispSecret'], luciEnabled
    raise LookupError("Didn't find AP.")


def renderBuildScript(buildInfo):
    env = jinja2.Environment(loader=jinja2.FileSystemLoader("templates"))
    template = env.get_template("openWRTbuild.sh")
    out = template.render(
        ap=buildInfo[0],
        openWISP_url=buildInfo[1],
        openWISP_secret=buildInfo[2],
        luciEnabled=buildInfo[3]
    )
    with open(builderScriptPath, 'w') as f:
        f.write(out)
    os.chmod(builderScriptPath, stat.S_IRWXU)


def getUserInputForConfig():
    questions = [
        inquirer.Text(
            'ip_self',
            message="Enter desired IP of the Access Point.",
            default="192.168.42.254",
        ),
        inquirer.Text(
            'ap_number',
            message="Enter desired lending number.",
            validate=lambda _, x: x.isnumeric()
        ),
        inquirer.Password(
            'root_password',
            message="Enter the root password."
        )
    ]
    answers = inquirer.prompt(questions)
    print(answers)

    return answers['ip_self'], answers['ap_number'], answers['root_password']


def renderConfig(ap, config):
    env = jinja2.Environment(
        loader=jinja2.FileSystemLoader(
            os.path.join(
                "templates",
                "uci-defaults")))
    template = env.get_template("99_baer")
    out = template.render(
        ip_self=config[0], ApNumber=config[1], root_password=config[2], ap=ap)

    with open(os.path.join('rendered', 'uci-defaults', '99_baer'), 'w') as f:
        f.write(out)


if __name__ == '__main__':
    buildInfo = getUserInputForBuild()
    renderBuildScript(buildInfo)
    config = getUserInputForConfig()
    renderConfig(buildInfo[0], config)
    subprocess.run(["bash", builderScriptPath])
